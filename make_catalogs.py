"""
This script reads in the mock data sets, extracts the photometry,
applies the completeness function, and writes the observable clusters
to a text file.
"""

import numpy as np
import os.path as osp
from slugpy import read_cluster

# Directory where data live
datadir = 'mockdata'

# List of mock catalogs
catalogs = [ 'mock_truncated', 'mock_powerlaw', 'mock_mdd',
             'mock_mismatch']

# List of filters
filters = ['WFC3_UVIS_F275W', 'WFC3_UVIS_F336W', 'WFC3_UVIS_F438W',
           'WFC3_UVIS_F555W', 'WFC3_UVIS_F814W']

# Photometric error value, in mag
photerr = 0.1

# Completeness function
def comp(Vmag):
    c = np.zeros(np.asarray(Vmag).shape)
    idx1 = np.asarray(Vmag) <= -5
    c[idx1] = 1.0
    idx2 = np.asarray(Vmag) >= -4
    c[idx2] = 0.0
    idx = np.logical_and(np.logical_not(idx1),
                         np.logical_not(idx2))
    c[idx] = -4.0 - np.asarray(Vmag)[idx]
    return c

# Another, different completeness function
def comp2(Vmag):
    c = np.zeros(np.asarray(Vmag).shape)
    idx1 = np.asarray(Vmag) <= -4.75
    c[idx1] = 1.0
    idx2 = np.asarray(Vmag) >= -4
    c[idx2] = 0.0
    idx = np.logical_and(np.logical_not(idx1),
                         np.logical_not(idx2))
    c[idx] = ((-4.0 - np.asarray(Vmag)[idx])/0.75)**2
    return c


# Loop over catalogs
for j, cat in enumerate(catalogs):

    # Manually set the seed so that we get the same results each time
    np.random.seed(43253)
    
    # Read data
    data = read_cluster(cat, output_dir=datadir)

    # Get magnitudes
    uv_mag_true = data.phot_neb_ex[:,data.filter_names.index(filters[0])]
    u_mag_true = data.phot_neb_ex[:,data.filter_names.index(filters[1])]
    b_mag_true = data.phot_neb_ex[:,data.filter_names.index(filters[2])]
    v_mag_true = data.phot_neb_ex[:,data.filter_names.index(filters[3])]
    i_mag_true = data.phot_neb_ex[:,data.filter_names.index(filters[4])]

    # Add errors
    uv_mag = uv_mag_true + np.random.normal(scale=photerr,
                                            size=len(uv_mag_true))
    u_mag = u_mag_true + np.random.normal(scale=photerr,
                                          size=len(u_mag_true))
    b_mag = b_mag_true + np.random.normal(scale=photerr,
                                          size=len(b_mag_true))
    v_mag = v_mag_true + np.random.normal(scale=photerr,
                                          size=len(v_mag_true))
    i_mag = i_mag_true + np.random.normal(scale=photerr,
                                          size=len(i_mag_true))

    # For first catalog only, make a version with double errors
    if j == 0:
        uv_mag_e2 = uv_mag_true + 2.0*(uv_mag - uv_mag_true)
        u_mag_e2 = u_mag_true + 2.0*(u_mag - u_mag_true)
        b_mag_e2 = b_mag_true + 2.0*(b_mag - b_mag_true)
        v_mag_e2 = v_mag_true + 2.0*(v_mag - v_mag_true)
        i_mag_e2 = i_mag_true + 2.0*(i_mag - i_mag_true)

    # Compute completeness
    c = comp(v_mag)

    # First first catalog, compute completeness using a 2nd
    # completeness function and for double error case
    if j == 0:
        c2 = comp2(v_mag)
        c_e2 = comp(v_mag_e2)

    # Figure out which clusters are observed
    idx_obs = np.zeros(c.shape, dtype='bool')
    idx_obs[c == 1.0] = True
    idx = np.logical_and(c > 0.0, c < 1.0)
    rnd = np.random.rand(np.sum(idx))
    idx_obs[idx] = rnd < c[idx]

    # If applicable, repeat for 2nd completeness function and double
    # error case
    if j == 0:
        idx_obs2 = np.zeros(c2.shape, dtype='bool')
        idx_obs2[c2 == 1.0] = True
        idx = np.logical_and(c2 > 0.0, c2 < 1.0)
        rnd = np.random.rand(np.sum(idx))
        idx_obs2[idx] = rnd < c2[idx]
        idx_obs_e2 = np.zeros(c_e2.shape, dtype='bool')
        idx_obs_e2[c_e2 == 1.0] = True
        idx = np.logical_and(c_e2 > 0.0, c_e2 < 1.0)
        rnd = np.random.rand(np.sum(idx))
        idx_obs_e2[idx] = rnd < c_e2[idx]

    # Write full cluster list
    print("Writing "+osp.join(datadir, cat) + "_full.txt -- {:d} clusters".
          format(len(uv_mag)))
    fp = open(osp.join(datadir, cat) + "_full.txt", "w")
    fp.write("# This is the full catalog, i.e., with no completeness cut.\n")
    fp.write("# Column 1: ID number\n")
    fp.write("# Columns 2 - 6: absolute Vega magnitudes in the specified filters\n")
    fp.write("# Columns 7 - 11: error in magnitude in the specified filers\n")
    fp.write("# Column 12: estimated observational completeness for this clusters's photometry\n")
    fp.write("#   ID")
    for i in range(len(filters)):
        fp.write("  {:>18s}".format(filters[i]))
    for i in range(len(filters)):
        fp.write("  {:>18s}".format(filters[i]+'_e'))
    fp.write("  {:>18s}".format("Completeness"))
    fp.write("\n")
    for i in range(len(uv_mag)):
        fp.write(
            "{:>6d}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}\n".
            format(i+1, uv_mag[i], u_mag[i], b_mag[i], v_mag[i], i_mag[i],
                   photerr, photerr, photerr, photerr, photerr, c[i]))
    fp.close()

    # Write full cluster list with no errors
    print("Writing "+osp.join(datadir, cat) + "_noerr.txt -- {:d} clusters".
          format(len(uv_mag)))
    fp = open(osp.join(datadir, cat) + "_noerr.txt", "w")
    fp.write("# This is the full catalog without errors and with no completeness cut.\n")
    fp.write("# Column 1: ID number\n")
    fp.write("# Columns 2 - 6: absolute Vega magnitudes in the specified filters\n")
    fp.write("# Columns 7 - 11: error in magnitude in the specified filers\n")
    fp.write("# Column 12: estimated observational completeness for this clusters's photometry\n")
    fp.write("#   ID")
    for i in range(len(filters)):
        fp.write("  {:>18s}".format(filters[i]))
    for i in range(len(filters)):
        fp.write("  {:>18s}".format(filters[i]+'_e'))
    fp.write("  {:>18s}".format("Completeness"))
    fp.write("\n")
    for i in range(len(uv_mag)):
        fp.write(
            "{:>6d}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}\n".
            format(i+1, uv_mag_true[i], u_mag_true[i], b_mag_true[i],
                   v_mag_true[i], i_mag_true[i], 0.0, 0.0, 0.0, 0.0, 0.0,
                   c[i]))
    fp.close()

    # Prune cluster lists
    if j == 0:
        id_obs2 = np.arange(len(uv_mag))[idx_obs2]+1
        uv_mag2 = uv_mag[idx_obs2]
        u_mag2 = u_mag[idx_obs2]
        b_mag2 = b_mag[idx_obs2]
        v_mag2 = v_mag[idx_obs2]
        i_mag2 = i_mag[idx_obs2]
        c2 = c2[idx_obs2]
        id_obs_e2 = np.arange(len(uv_mag_e2))[idx_obs_e2]+1
        uv_mag_e2 = uv_mag_e2[idx_obs_e2]
        u_mag_e2 = u_mag_e2[idx_obs_e2]
        b_mag_e2 = b_mag_e2[idx_obs_e2]
        v_mag_e2 = v_mag_e2[idx_obs_e2]
        i_mag_e2 = i_mag_e2[idx_obs_e2]
        c_e2 = c_e2[idx_obs_e2]
    id_obs = np.arange(len(uv_mag))[idx_obs]+1
    uv_mag = uv_mag[idx_obs]
    u_mag = u_mag[idx_obs]
    b_mag = b_mag[idx_obs]
    v_mag = v_mag[idx_obs]
    i_mag = i_mag[idx_obs]
    c = c[idx_obs]

    # Dump data to file
    print("Writing "+osp.join(datadir, cat) + "_obs.txt -- {:d} clusters".
          format(len(uv_mag)))
    fp = open(osp.join(datadir, cat) + "_obs.txt", "w")
    fp.write("# This is the observed catalog, taking into account incompleteness\n")
    fp.write("# Column 1: ID number\n")
    fp.write("# Columns 2 - 6: absolute Vega magnitudes in the specified filters\n")
    fp.write("# Columns 7 - 11: error in magnitude in the specified filers\n")
    fp.write("# Column 12: estimated observational completeness for this clusters's photometry\n")
    fp.write("#   ID")
    for i in range(len(filters)):
        fp.write("  {:>18s}".format(filters[i]))
    for i in range(len(filters)):
        fp.write("  {:>18s}".format(filters[i]+'_e'))
    fp.write("  {:>18s}".format("Completeness"))
    fp.write("\n")
    for i in range(len(uv_mag)):
        fp.write(
            "{:>6d}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}\n".
            format(id_obs[i], uv_mag[i], u_mag[i], b_mag[i], v_mag[i], i_mag[i],
                   photerr, photerr, photerr, photerr, photerr, c[i]))
    fp.close()

    # Dump 2nd completeness data to file
    if j == 0:
        print("Writing "+osp.join(datadir, 'mock_comperr') + "_obs.txt -- {:d} clusters".
              format(len(uv_mag2)))
        fp = open(osp.join(datadir, 'mock_comperr') + "_obs.txt", "w")
        fp.write("# This is the observed catalog, taking into account incompleteness\n")
        fp.write("# Column 1: ID number\n")
        fp.write("# Columns 2 - 6: absolute Vega magnitudes in the specified filters\n")
        fp.write("# Columns 7 - 11: error in magnitude in the specified filers\n")
        fp.write("# Column 12: estimated observational completeness for this clusters's photometry\n")
        fp.write("#   ID")
        for i in range(len(filters)):
            fp.write("  {:>18s}".format(filters[i]))
        for i in range(len(filters)):
            fp.write("  {:>18s}".format(filters[i]+'_e'))
        fp.write("  {:>18s}".format("Completeness"))
        fp.write("\n")
        for i in range(len(uv_mag2)):
            fp.write(
            "{:>6d}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}\n".
                format(id_obs2[i], uv_mag2[i], u_mag2[i], b_mag2[i], v_mag2[i], i_mag2[i],
                       photerr, photerr, photerr, photerr, photerr, c2[i]))
        fp.close()

    # Dump double error data to file
    if j == 0:
        print("Writing "+osp.join(datadir, 'mock_err2') + "_obs.txt -- {:d} clusters".
              format(len(uv_mag_e2)))
        fp = open(osp.join(datadir, 'mock_err2') + "_obs.txt", "w")
        fp.write("# This is the observed catalog, taking into account incompleteness\n")
        fp.write("# Column 1: ID number\n")
        fp.write("# Columns 2 - 6: absolute Vega magnitudes in the specified filters\n")
        fp.write("# Columns 7 - 11: error in magnitude in the specified filers\n")
        fp.write("# Column 12: estimated observational completeness for this clusters's photometry\n")
        fp.write("#   ID")
        for i in range(len(filters)):
            fp.write("  {:>18s}".format(filters[i]))
        for i in range(len(filters)):
            fp.write("  {:>18s}".format(filters[i]+'_e'))
        fp.write("  {:>18s}".format("Completeness"))
        fp.write("\n")
        for i in range(len(uv_mag_e2)):
            fp.write(
            "{:>6d}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}  {:>18.9f}\n".
                format(id_obs_e2[i], uv_mag_e2[i], u_mag_e2[i], b_mag_e2[i], v_mag_e2[i], i_mag_e2[i],
                       2*photerr, 2*photerr, 2*photerr, 2*photerr, 2*photerr, c_e2[i]))
        fp.close()

