###### Select resources #####
#PBS -N mock_powerlaw_mdd
#PBS -l ncpus=56
##### Queue #####
#PBS -q smallmem
##### Mail Options #####
# Send an email at job start, end and if aborted
#PBS -m abe
##### Change to your working directory #####
cd /home/krumholz/Projects/slug4
##### Execute Program #####
/pkg/linux/anaconda/bin/python ./analyze_catalog.py LIBDIR/kafc18 LIBDIR/lib_mass.pdf LIBDIR/lib_time.pdf LIBDIR/lib_av.pdf mockdata/mock_powerlaw_obs.txt --cattype mock --verbose --niter 500 --bwphot 0.05 --outname chains/mock_powerlaw_obs_mdd.chain --bwphys 0.05 --mdd > mock_powerlaw_obs_mdd.log

