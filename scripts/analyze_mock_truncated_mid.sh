###### Select resources #####
#PBS -N mock_truncated_mid
#PBS -l ncpus=56
##### Queue #####
#PBS -q largemem
##### Mail Options #####
# Send an email at job start, end and if aborted
#PBS -m abe
##### Change to your working directory #####
cd /home/krumholz/Projects/slug4
##### Execute Program #####
/pkg/linux/anaconda/bin/python ./analyze_catalog.py LIBDIR/kafc18 LIBDIR/lib_mass.pdf LIBDIR/lib_time.pdf LIBDIR/lib_av.pdf mockdata/mock_truncated_obs.txt --cattype mock --verbose --niter 500 --bwphot 0.05 --outname chains/mock_truncated_obs_mid.chain --bwphys 0.05 > mock_truncated_obs_mid.log
