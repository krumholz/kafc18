"""
This script does conventional chi^2 fitting to the binned mock
catalogs, and produces plots of the results.
"""

import numpy as np
import matplotlib.pyplot as plt
import os.path as osp
from scipy.special import expn
from scipy.optimize import curve_fit
from slugpy import read_cluster_prop
from pdfplot import pdfplot

# Mass functions to fit
def dn_dlogm_fit(logm, norm, alphaM, logMbreak):
    m = 10.**logm
    mbreak = 10.**logMbreak
    dndlogm = norm * m**(1.0+alphaM) * np.exp(-m/mbreak)
    return dndlogm
def dn_dlogm_pl_fit(logm, norm, alphaM):
    m = 10.**logm
    dndlogm = norm * m**(1.0+alphaM)
    return dndlogm

# Age distribution to fit
def dn_dlogt_fit(logt, norm, alphaT, logTmid):
    t = 10.**logt
    Tmid = 10.**logTmid
    dndlogt = np.zeros(len(logt))
    idx = t <= Tmid
    dndlogt[idx] = norm * t[idx]/Tmid
    idx = t > Tmid
    dndlogt[idx] = norm * (t[idx]/Tmid)**(1.0+alphaT)
    return dndlogt

# Directories where mock catalogs and the fits to them live
mockdir = 'mockdata'
fitdir = 'conventional'

# Cut mass and age
mcut = 10.**3.75
tcut = 10.**8.5

# chi^2 cut
rcs_max = 5.0

# Load mock data and fits to them
catalogs = ['powerlaw', 'truncated', 'mdd']
alphaM_true = [-2, -2, -2]
logMb_true = [6.5, 5, 5]
alphaT_true = [-1, -1, 0]
logTmid_true = [6.5, 8.0, 0]
fitdata = {}
for c, alphaM, logMb, alphaT, logTmid in \
    zip(catalogs, alphaM_true, logMb_true, alphaT_true, logTmid_true):
    
    # Mock data
    data = np.loadtxt(osp.join(fitdir, c+'_fit.txt'))
    cid = np.array(data[:,0], dtype='int')
    logm = data[:,2]
    logt = data[:,5]
    rcs = data[:,-1]

    # Apply mass = / age cuts
    keep = np.logical_and(logm > np.log10(mcut), logt < np.log10(tcut))
    logm_cut = logm[keep]
    logt_cut = logt[keep]
    print("Catalog "+c+":")
    print("   {:d} / {:d} clusters meet mass/age cut".
          format(len(logm_cut), len(logm)))

    # Apply chi^2 cuts
    logm_good = logm[rcs < rcs_max]
    logt_good = logt[rcs < rcs_max]
    print("   {:d} / {:d} clusters meet chi^2 cut".
          format(len(logm_good), len(logm)))
    keep_good = np.logical_and.reduce(
        (logm > np.log10(mcut), logt < np.log10(tcut), rcs < rcs_max))
    logm_cut_good = logm[keep_good]
    logt_cut_good = logt[keep_good]
    print("   {:d} / {:d} clusters meet chi^2 and mass/age cut".
          format(len(logm_cut_good), len(logm)))
    
    # True input values
    data=read_cluster_prop(osp.join(mockdir, 'mock_'+c))
    logm_true = np.log10(data.target_mass[cid-1])
    logt_true = np.log10(data.time[cid-1])
    logm_cut_true = logm_true[keep]
    logt_cut_true = logt_true[keep]
    logm_good_true = logm_true[rcs < rcs_max]
    logt_good_true = logt_true[rcs < rcs_max]
    logm_cut_good_true = logm_true[keep_good]
    logt_cut_good_true = logt_true[keep_good]
   
    # Bin the data
    logmbin = np.log10(mcut)+0.5*np.arange(7)
    logmc = np.log10(mcut)+0.5*np.arange(6)+0.25
    logtbin = np.array([-np.inf, 6.5, 7, 7.5, 8, 8.5])
    logtc = np.array([6.25, 6.75, 7.25, 7.75, 8.25])
    hist, logme, logte \
     = np.histogram2d(logm_cut_good, logt_cut_good, bins=[logmbin, logtbin])
        
    # Fit the mass distributions
    cnts = np.sum(hist, axis=1)
    cnts_err = np.sqrt(cnts)
    cnts_err[cnts_err == 0] = 1.0
    pm, covm = curve_fit(dn_dlogm_fit, logmc, cnts, 
                         sigma=cnts_err,
                         p0=[1000., -2.0, 5.0])
    rchisq = np.sum((cnts - 
                     dn_dlogm_fit(logmc, pm[0], pm[1], pm[2]))**2 / 
                    cnts_err**2) / (len(cnts)-3)
    pm_pl, covm_pl = curve_fit(dn_dlogm_pl_fit, logmc, cnts, 
                               sigma=cnts_err,
                               p0=[1000., -2.0])
    rchisq_pl = np.sum((cnts - 
                        dn_dlogm_pl_fit(logmc, pm_pl[0], pm_pl[1]))**2 / 
                       cnts_err**2) / (len(cnts)-2)
    
    # Fit the age distribution
    cnts = np.sum(hist, axis=0)
    cnts_err = np.sqrt(cnts)
    cnts_err[cnts_err == 0] = 1.0
    pt, covt = curve_fit(dn_dlogt_fit, logtc, cnts,
                        sigma=cnts_err,
                        p0=[1000., -1.0, 6.5])
    rchisqT = np.sum((cnts - 
                      dn_dlogt_fit(logtc, pt[0], pt[1], pt[2]))**2 / 
                     cnts_err**2) / (len(cnts)-3)
    
    # Store data
    f = { 'cid' : cid ,
          'logm' : logm,
          'logt' : logt,
          'logm_cut' : logm_cut,
          'logt_cut' : logt_cut,
          'logm_good' : logm_good,
          'logt_good' : logt_good,
          'logm_cut_good' : logm_cut_good,
          'logt_cut_good' : logt_cut_good,
          'logm_true' : logm_true,
          'logt_true' : logt_true,
          'logm_cut_true' : logm_cut_true,
          'logt_cut_true' : logt_cut_true,
          'logm_good_true' : logm_good_true,
          'logt_good_true' : logt_good_true,
          'logm_cut_good_true' : logm_cut_good_true,
          'logt_cut_good_true' : logt_cut_good_true,
          'normM'  : pm[0], 
          'alphaM' : pm[1],
          'logMbreak' : pm[2],
          'alphaM_disp' : np.sqrt(covm[1,1]),
          'logMbreak_disp' : np.sqrt(covm[2,2]),
          'normM_pl'  : pm_pl[0], 
          'alphaM_pl' : pm_pl[1],
          'alphaM_pl_disp' : np.sqrt(covm_pl[1,1]),
          'rchisqM' : rchisq,
          'rchisqM_pl' : rchisq_pl,
          'normT' : pt[0],
          'alphaT' : pt[1],
          'logTmid' : pt[2],
          'alphaT_disp' : np.sqrt(covt[1,1]),
          'logTmid_disp' : np.sqrt(covt[2,2]),
          'rchisqT' : rchisqT,
          'alphaM_true' : alphaM,
          'logMb_true' : logMb,
          'alphaT_true' : alphaT,
          'logTmid_true' : logTmid,
          'hist' : hist }
    fitdata[c] = f

# Plot true versus fitted mass
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

# All good fits
fig = plt.figure(1,figsize=(5,5))
axcen, axtop, axright \
    = pdfplot(fitdata['powerlaw']['logm_good_true'],
              fitdata['powerlaw']['logm_good'],
              fignum=1,
              nxbin=18, nybin=18, 
              thresh=10.**-1.5,
              zscaling='max',
              xlabel=r'$\log\,M_{\mathrm{true}}$ [$M_\odot$]',
              ylabel=r'$\log\,M_{\mathrm{fit}}$ [$M_\odot$]',
              xlim=[2,6.5], 
              ylim=[2,6.5],
              xhistlim=[1e-3,3],
              yhistlim=[1e-3,3],
              zmax=1.0,
              nticks=4,
              scat_alpha=0.5)
axcen.plot([2,6.5], [2,6.5], 'k-', alpha=0.5)
axcen.text(4,6,'All', horizontalalignment='center')
plt.savefig('fit_mass_powerlaw_all.pdf')

# After applying mass and age cut
fig = plt.figure(2,figsize=(5,5))
axcen, axtop, axright \
    = pdfplot(fitdata['powerlaw']['logm_cut_good_true'],
              fitdata['powerlaw']['logm_cut_good'],
              fignum=2,
              nxbin=18, nybin=18, 
              thresh=10.**-1.5,
              zscaling='max',
              xlabel=r'$\log\,M_{\mathrm{true}}$ [$M_\odot$]',
              ylabel=r'$\log\,M_{\mathrm{fit}}$ [$M_\odot$]',
              xlim=[2,6.5], 
              ylim=[2,6.5],
              xhistlim=[1e-3,3],
              yhistlim=[1e-3,3],
              zmax=1.0,
              nticks=4,
              scat_alpha=0.5)
axcen.plot([2,6.5], [2,6.5], 'k-', alpha=0.5)
axcen.text(4,6,'Mass / age cut', horizontalalignment='center')
axcen.fill_between([2, 6.5], [3.75, 3.75], color='k', alpha=0.25)
axcen.fill_between([2, 3.75], [6.5, 6.5], color='k', alpha=0.25)
axtop.fill_between([2, 3.75], [3,3], color='k', alpha=0.25)
axright.fill_between([1e-3,3], [3.75, 3.75], color='k', alpha=0.25)
plt.savefig('fit_mass_powerlaw_cut.pdf')


# Plot true versus fitted age
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

# All good fits
fig = plt.figure(1,figsize=(5,5))
axcen, axtop, axright \
    = pdfplot(fitdata['powerlaw']['logt_good_true'],
              fitdata['powerlaw']['logt_good'],
              fignum=1,
              nxbin=10, nybin=10, 
              thresh=10.**-1.5,
              zscaling='max',
              xlabel=r'$\log\,T_{\mathrm{true}}$ [yr]',
              ylabel=r'$\log\,T_{\mathrm{fit}}$ [yr]',
              xlim=[5,10], 
              ylim=[5,10],
              xhistlim=[3e-2,3],
              yhistlim=[3e-2,3],
              zmax=1.0,
              nticks=4,
              scat_alpha=0.5)
axcen.plot([5,10], [5,10], 'k-', alpha=0.5)
axcen.text(5.5, 9, 'All', horizontalalignment='left')
plt.savefig('fit_age_powerlaw_all.pdf')

# After applying mass and age cut
fig = plt.figure(2,figsize=(5,5))
axcen, axtop, axright \
    = pdfplot(fitdata['powerlaw']['logt_cut_good_true'],
              fitdata['powerlaw']['logt_cut_good'],
              fignum=2,
              nxbin=10, nybin=10, 
              thresh=10.**-1.5,
              zscaling='max',
              xlabel=r'$\log\,T_{\mathrm{true}}$ [yr]',
              ylabel=r'$\log\,T_{\mathrm{fit}}$ [yr]',
              xlim=[5,10], 
              ylim=[5,10],
              xhistlim=[3e-2,3],
              yhistlim=[3e-2,3],
              zmax=1.0,
              nticks=4,
              scat_alpha=0.5)
axcen.plot([5,10], [5,10], 'k-', alpha=0.5)
axcen.text(5.5, 9, 'Mass / age cut', horizontalalignment='left')
axcen.fill_between([5, 10], [10,10], [8.5, 8.5], color='k', alpha=0.25)
axcen.fill_between([8.5, 10], [10,10], color='k', alpha=0.25)
axtop.fill_between([8.5, 10], [3,3], color='k', alpha=0.25)
axright.fill_between([3e-2,3], [10, 10], [8.5, 8.5], color='k', alpha=0.25)
plt.savefig('fit_age_powerlaw_cut.pdf')


# Make plot of binned mass distributions, and print out fit results
fig = plt.figure(1, figsize=(4,7))
plt.clf()
for i, c in enumerate(catalogs):
    # Print best fit values
    print("Catalog {:s}".format(c))
    print("   Truncated fit: alphaM = {:f} +- {:f}, logMbr = {:f} +- {:f}, reduced chi^2 = {:f}".
          format(fitdata[c]['alphaM'], fitdata[c]['alphaM_disp'],
                 fitdata[c]['logMbreak'], fitdata[c]['logMbreak_disp'],
                 fitdata[c]['rchisqM']))
    print("   Pure powerlaw fit: alphaM = {:f} +- {:f}, reduced chi^2 = {:f}".
          format(fitdata[c]['alphaM_pl'], fitdata[c]['alphaM_pl_disp'],
                 fitdata[c]['rchisqM_pl']))

    # Plot all ages
    ax = plt.subplot(3,1,i+1)
    cnt = np.sum(fitdata[c]['hist'], axis=1)
    cnt_err = np.sqrt(cnt)
    cnt_err[cnt_err == 0] = 1.0
    plt.errorbar(logmc, cnt+1e-6, xerr=0.25, yerr=cnt_err, fmt='none',
                 color='C0')
    plts=[]
    labels=[]
    p,=plt.plot(logmc, cnt, 'o', color='C0', ms=10)
    plts.append(p)
    labels.append('All ages')
    
    # Plot data separated by age
    cnt = np.sum(fitdata[c]['hist'][:,:2], axis=1)
    p,=plt.plot(logmc-0.1, cnt, 'o', mec='C0', mfc='none', ms=10)
    plts.append(p)
    labels.append(r'$<10$ Myr')
    cnt = np.sum(fitdata[c]['hist'][:,2:4], axis=1)
    p,=plt.plot(logmc, cnt, 'o', mec='C1', mfc='none', ms=10)
    plts.append(p)
    labels.append(r'$10-100$ Myr')
    cnt = np.sum(fitdata[c]['hist'][:,4:], axis=1)
    p,=plt.plot(logmc+0.1, cnt, 'o', mec='C2', mfc='none', ms=10)
    plts.append(p)
    labels.append(r'$>100$ Myr')
    lmc = np.linspace(3.75, 6.75)
    p,=plt.plot(lmc, dn_dlogm_fit(lmc, fitdata[c]['normM'], 
                                  fitdata[c]['alphaM'],
                                  fitdata[c]['logMbreak']),
                'k')
    plts.append(p)
    labels.append('Best truncated')
    p,=plt.plot(lmc, dn_dlogm_pl_fit(lmc, fitdata[c]['normM_pl'], 
                                     fitdata[c]['alphaM_pl']),
                'k--')
    plts.append(p)
    labels.append('Best powerlaw')
    true_fit = dn_dlogm_fit(lmc, fitdata[c]['normM'],
                            fitdata[c]['alphaM_true'],
                            fitdata[c]['logMb_true'])
    idx = np.argmax(lmc > 4)
    cnt = np.sum(fitdata[c]['hist'], axis=1)
    true_fit *= cnt[0]/true_fit[idx]
    if i != 2:
        p,=plt.plot(lmc, true_fit, 'k:')
        plts.append(p)
        labels.append('True')
    
    # Add title and legend
    if i==0:
        plt.legend(plts[:4], labels[:4], loc='lower left')
    elif i==1:
        plt.legend(plts[4:], labels[4:], loc='lower left')
    plt.text(6.5, 200, '\\texttt{'+c.title()+'}', horizontalalignment='right')
        
    # Tweak plot
    plt.yscale('log')
    plt.xlim([3.75, 6.75])
    plt.ylim([0.1,8e2])
    if i != 2:
        ax.set_xticklabels('')
    else:
        ax.set_xlabel(r'$\log M$ [$M_\odot$]')
    if i == 1:
        ax.set_ylabel('Number of clusters')

# Tweak plot
plt.subplots_adjust(hspace=0.05, top=0.96, left=0.18, bottom=0.08)

# Save
plt.savefig('mfit_bins.pdf')


# Make plot of binned age distributions, and print out fit results
fig = plt.figure(1, figsize=(4,7))
plt.clf()
for i, c in enumerate(catalogs):
    # Print best fit values
    print("Catalog {:s}".format(c))
    print("   Age distribution fit: alphaT = {:f} +- {:f}, logTmid = {:f} +- {:f}, reduced chi^2 = {:f}".
          format(fitdata[c]['alphaT'], fitdata[c]['alphaT_disp'],
                 fitdata[c]['logTmid'], fitdata[c]['logTmid_disp'],
                 fitdata[c]['rchisqT']))

    # Plot all ages
    ax = plt.subplot(3,1,i+1)
    cnt = np.sum(fitdata[c]['hist'], axis=0)
    cnt_err = np.sqrt(cnt)
    cnt_err[cnt_err == 0] = 1.0
    plt.errorbar(logtc, cnt+1e-6, xerr=0.25, yerr=cnt_err, fmt='none',
                 color='C0')
    plts=[]
    labels=[]
    p,=plt.plot(logtc, cnt, 'o', color='C0', ms=10)
    plts.append(p)
    labels.append('All masses')
    
    # Plot data separated by age
    cnt = np.sum(fitdata[c]['hist'][:1,:], axis=0)
    p,=plt.plot(logtc-0.1, cnt, 'o', mec='C0', mfc='none', ms=10)
    plts.append(p)
    labels.append('$<10^{'+'{:4.2f}'.format(logme[1])+'}$ $M_\odot$')
    cnt = np.sum(fitdata[c]['hist'][1:2,:], axis=0)
    p,=plt.plot(logtc, cnt, 'o', mec='C1', mfc='none', ms=10)
    plts.append(p)
    labels.append('$10^{'+'{:4.2f}'.format(logme[1])+
                  '-'+'{:4.2f}'.format(logme[2])+'}$ $M_\odot$')
    cnt = np.sum(fitdata[c]['hist'][2:,:], axis=0)
    p,=plt.plot(logtc+0.1, cnt, 'o', mec='C2', mfc='none', ms=10)
    plts.append(p)
    labels.append('$>10^{'+'{:4.2f}'.format(logme[2])+'}$ $M_\odot$')
    ltc = np.linspace(6, 8.5)
    p,=plt.plot(ltc, dn_dlogt_fit(ltc, fitdata[c]['normT'], 
                                  fitdata[c]['alphaT'],
                                  fitdata[c]['logTmid']),
                'k')
    plts.append(p)
    labels.append('Best fit')
    true_fit = dn_dlogt_fit(ltc, fitdata[c]['normT'],
                            fitdata[c]['alphaT_true'],
                            fitdata[c]['logTmid_true'])
    idx = np.argmax(ltc >= logtc[0])
    cnt = np.sum(fitdata[c]['hist'], axis=0)
    true_fit *= cnt[0]/true_fit[idx]
    if i != 2:
        p,=plt.plot(ltc, true_fit, 'k:')
        plts.append(p)
        labels.append('True')
    
    # Add title and legend
    if i==0:
        plt.legend(plts[:4], labels[:4], loc='lower left')
    elif i==1:
        plt.legend(plts[4:], labels[4:], loc='lower left')
    plt.text(8.25, 0.2, '\\texttt{'+c.title()+'}', horizontalalignment='right')
        
    # Tweak plot
    plt.yscale('log')
    plt.xlim([6, 8.5])
    plt.ylim([0.1,8e2])
    if i != 2:
        ax.set_xticklabels('')
    else:
        ax.set_xlabel(r'$\log T$ [yr]')
    if i == 1:
        ax.set_ylabel('Number of clusters')

# Tweak plot
plt.subplots_adjust(hspace=0.05, top=0.96, left=0.18, bottom=0.08)

# Save
plt.savefig('tfit_bins.pdf')
