"""
This script contains some code to do cleanup on the LEGUS
catalogs. The cleaning required is that (1) we need to remove clusters
that appear in more than one catalog; (2) remove clusters that
nominally have zero change of being observed, which can get into the
sample anyway due to peculiarities of the way that the magnitude limit
for visual classification was combined with the aperture correction
"""

import numpy as np

def clean_legus(catalogs, verbose):
    """
    This cleans up LEGUS catalogs, getting the data into a uniform
    state suitable for Bayesian analysis

    Parameters
       catalogs : list
          catalogs prepared by the analyze_catalog script
       verbose : bool
          print verbose output or not

    Returns
       nothing
    """

    # First identify clusters that nominally have zero completeness;
    # remove them from both the sorted and original photometry lists
    for c in catalogs:
        ndel = 0
        for i in range(len(c['phot_filterset'])):
            comp = c['comp'][i].comp(c['phot_filterset'][i])
            idx_del = comp == 0.0
            idx_keep = comp > 0
            cids_to_delete = c['cid_filterset'][i][idx_del]
            c['cid_filterset'][i] = c['cid_filterset'][i][idx_keep]
            c['phot_filterset'][i] = c['phot_filterset'][i][idx_keep]
            c['photerr_filterset'][i] = c['photerr_filterset'][i][idx_keep]
            ndel += len(cids_to_delete)
            for cid in cids_to_delete:
                idx_keep = c['cid'] != cid
                c['cid'] = c['cid'][idx_keep]
                c['phot'] = c['phot'][idx_keep]
                c['photerr'] = c['photerr'][idx_keep]
                c['detect'] = c['detect'][idx_keep]
                c['filtersets_index'] = c['filtersets_index'][idx_keep]
                c['ra'] = c['ra'][idx_keep]
                c['dec'] = c['dec'][idx_keep]
        if verbose and ndel > 0:
            print(("   [clean_legus]: removed {:d} clusters from "
                   "catalog {:s} due to completeness = 0 issue").
                  format(ndel, c['basename']))

    # Second remove clusters that are duplicated across catalogs
    for i, c in enumerate(catalogs):

        # Compare every RA and DEC in every other catalog to find
        # duplicates
        dup = np.zeros(len(c['ra']), dtype=bool)
        for c2 in catalogs[i+1:]:
            ra_match = np.equal.outer(c['ra'], c2['ra'])
            dec_match = np.equal.outer(c['dec'], c2['dec'])
            dup_cat = np.logical_or.reduce(
                np.logical_and(ra_match, dec_match), axis=1)
            dup = np.logical_or(dup, dup_cat)

        # Delete duplicates
        idx_keep = np.logical_not(dup)
        cids_to_delete = c['cid'][dup]
        ndel = len(cids_to_delete)
        c['cid'] = c['cid'][idx_keep]
        c['phot'] = c['phot'][idx_keep]
        c['photerr'] = c['photerr'][idx_keep]
        c['detect'] = c['detect'][idx_keep]
        c['filtersets_index'] = c['filtersets_index'][idx_keep]
        for cid in cids_to_delete:
            for i in range(len(c['filtersets'])):
                idx_keep = c['cid_filterset'][i] != cid
                c['cid_filterset'][i] = c['cid_filterset'][i][idx_keep]
                c['phot_filterset'][i] = c['phot_filterset'][i][idx_keep]
                c['photerr_filterset'][i] = c['photerr_filterset'][i][idx_keep]

        # Print message
        if verbose and ndel > 0:
            print(("   [clean_legus]: removed {:d} clusters from "
                   "catalog {:s} due to duplication").
                  format(ndel, c['basename']))
            
    # Recalculate total number of clusters and return
    ncl = 0
    for c in catalogs:
        ncl += len(c['phot'])
    return ncl
