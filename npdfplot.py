"""
This utility makes corner plots in the style of Daniel
Foreman-Mackey's corner package, but in a style I find a bit nicer.
"""

import numpy as np
from scipy.stats import binned_statistic_2d
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.gridspec as gridspec

def npdfplot(pts, fignum=None, lolim=None, hilim=None,
             nbin=50, thresh=None, threshfrac=0.99,
             log=True, zmax=None, zscaling='max',
             histlim=None, colorbar=True, nticks=None,
             labels=None, scat_alpha=1.0,
             truths=None):
    """
    Makes a corner plot in the style of Daniel Foreman-Mackey's corner
    package, but using a somewhat different style.

    Parameters:
       pts : array, shape (N,M)
          array of points to be used to generate the plot; there are N
          points in M dimensions
       fignum : int
          figure number in which to create the plot
       lolim : arraylike, shape (N,)
          lower limits to be used for plotting in each dimension;
          default is minimum of data
       hilim : arraylike, shape (N,)
          upper limits to be used for plotting in each dimension;
          default is maximum of data
       nbin : int or arraylike, shape (N,)
          number of bins for histograms; if set to an int, the number
          is the same in each dimension, while if set to a list or
          array, this specifies the number of bins in each dimension
       thresh : float
          threshhold below which to hide the density histogram and
          show individual points; default is to use threshfrac instead
       threshfrac : float
          minimum fraction of the points that should be within the
          density threshhold; points in lower density regions are shown
          individually in a scatter plot
       log : bool
          use linear or logarithmic scale for PDFs
       zmax : float
          maximum value for 2D density PDF plots; default if this is
          not set depends on zscaling
       zscaling : 'max' | 'count' | 'frac' | 'density' | 'normed'
          method used to scale the PDFs; 'max' means that all
          histograms / PDFs are normalised to have a maximum of unity,
          'count' means that histograms / PDFs show absolute number
          counts in each bin, 'frac' means that histograms / PDFs show
          the fraction of the points in each bin, 'density' means
          that histograms / PDFs show the density of points in each
          bin, and 'normed' means that histograms / PDFs show the
          probability density in each bin
       histlim : arraylike, shape (M,2)
          upper and lower limits on the flanking histograms
       colorbar : bool
          include a color bar for the density 2D PDFs or not
       nticks : int
          number of tick marks on the color bar
       label : arraylike of string, shape (M,)
          labels for each dimension
       scat_alpha : float
          alpha value for scatter plot points
       truths : arraylike, shape (M,)
          true values of parameters, which should be highlighted

    Returns
       Nothing
    """

    # Get number of dimensions in x
    ndim = np.asarray(pts).shape[-1]

    # Holders for plotting data for variable pairs
    zarr = []
    zminarr = []
    zmaxarr = []
    binidxarr = []
    histarr = []
    scatteridxarr = []

    # First pass over dimension pairs to do binning
    for i in range(ndim):
    
        # Get x limits and bins
        x = pts[:,i]
        xlim = np.zeros(2)
        if hasattr(nbin, '__iter__'):
            nxbin = nbin[i]
        else:
            nxbin = nbin
        if lolim is None:
            xlim[0] = np.amin(x)
        else:
            xlim[0] = lolim[i]
        if hilim is None:
            xlim[1] = np.amax(x)
        else:
            xlim[1] = hilim[i]

        # Do 1D binning
        hist, xe \
            = np.histogram(x, bins=nxbin, range=xlim)

        # Apply scaling and save
        if zscaling == 'max':
            if np.amax(hist) == 0:
                raise ValueError("requested scaling is 'max', but all bins have zero count in some dimensions")
            histarr.append(hist / float(np.amax(hist)))
        elif zscaling == 'count':
            histarr.append(hist)
        elif zscaling == 'frac':
            histarr.append(hist / float(len(pts)))
        elif zscaling == 'density':
            histarr.append(hist / (xe[1]-xe[0]))
        elif zscaling == 'normed':
            histarr.append(hist / (len(pts)*(xe[1]-xe[0])))
        else:
            raise ValueError("bad value of zscaling")

        # Loop in j direction
        for j in range(i+1,ndim):

            # Get y limits and bins
            y = pts[:,j]
            ylim = np.zeros(2)
            if hasattr(nbin, '__iter__'):
                nybin = nbin[j]
            else:
                nybin = nbin
            if lolim is None:
                ylim[0] = np.amin(y)
            else:
                ylim[0] = lolim[j]
            if hilim is None:
                ylim[1] = np.amax(y)
            else:
                ylim[1] = hilim[j]

            # Make 2D histogram
            count, xe, ye, binidx \
                = binned_statistic_2d(x, y,
                                      np.ones(x.shape),
                                      statistic='sum',
                                      bins=[nxbin, nybin],
                                      range = [[xlim[0], xlim[1]],
                                               [ylim[0], ylim[1]]],
                                      expand_binnumbers=True)

            # Set z
            if zscaling == 'max':
                if np.amax(count) == 0:
                    raise ValueError("requested scaling is 'max', but all bins have zero count in some dimensions")
                z = count / np.amax(count)
            elif zscaling == 'count':
                z = count
            elif zscaling == 'frac':
                z = count / len(pts)
            elif zscaling == 'density':
                z = count / ((xe[1]-xe[0])*(ye[1]-ye[0]))
            elif zscaling == 'normed':
                z = count / (len(pts)*(xe[1]-xe[0])*(ye[1]-ye[0]))

            # Save
            zarr.append(z)
            binidxarr.append(binidx)

    # Second pass: set maxima and minima for each dimension pair
    for z in zip(zarr):

        # Set z maxima
        if zmax is not None:
            zmaxarr.append(zmax)
        elif zscaling == 'max':
            zmaxarr.append(1.0)
        else:
            zmaxarr.append(np.amax(z))

        # Set z minima
        if thresh is not None:
            zminarr.append(thresh)
        else:
            zsort = np.sort(z, axis=None)
            csum = np.cumsum(zsort)
            csum = csum/csum[-1]
            zminarr.append(zsort[np.argmax(csum > 1.0-threshfrac)])

    # Homogenize minima and maxima
    zmax = np.amax(zmaxarr)
    zmin = np.amin(zminarr)
    if log:
        zmax = np.log10(zmax)
        zmin = np.log10(zmin)
    
    # Third pass: figure out which points to show as scatter points
    for z, binidx in zip(zarr, binidxarr):
    
        # Figure out which points are within the binned region but
        # below the z minimum; save those positions
        flag = np.logical_and.reduce((binidx[0,:] > 0,
                                      binidx[1,:] > 0,
                                      binidx[0,:] <= count.shape[0],
                                      binidx[1,:] <= count.shape[1]))
        scatteridx = np.zeros(len(x), dtype=bool)
        if log:
            scatteridx[flag] \
                = z[binidx[0,flag]-1, binidx[1,flag]-1] < 10.**zmin
        else:
            scatteridx[flag] \
                = z[binidx[0,flag]-1, binidx[1,flag]-1] < zmin
        scatteridxarr.append(scatteridx)

    # Set up plot
    fig = plt.figure(fignum)
    plt.clf()
    gs = gridspec.GridSpec(ndim, ndim)
    
    # Third pass: loop over dimension pairs, this time to make plot
    ctr = 0
    for i in range(ndim):

        # Make grid in x direction
        x = pts[:,i]
        xlim = np.zeros(2)
        if hasattr(nbin, '__iter__'):
            nxbin = nbin[i]
        else:
            nxbin = nbin
        if lolim is None:
            xlim[0] = np.amin(x)
        else:
            xlim[0] = lolim[i]
        if hilim is None:
            xlim[1] = np.amax(x)
        else:
            xlim[1] = hilim[i]
        xgrd = np.linspace(xlim[0], xlim[1], nxbin+1)
        xgrd_h = 0.5*(xgrd[1:]+xgrd[:-1])

        # Make 1d histogram at top
        ax = plt.subplot(gs[i, i])
        ax.bar(xgrd[:-1], histarr[i], xgrd[1]-xgrd[0],
               align='edge', facecolor='C0', edgecolor='black')
        ax.set_xlim(xlim)
        if log:
            ax.set_yscale('log')

        # Set histogram limits
        if histlim is not None:
            if np.asarray(histlim).ndim == 2:
                ax.set_ylim(np.asarray(histlim)[i,:])
            else:
                ax.set_ylim(histlim)

        # Add truths line
        if truths is not None:
            ax.plot([truths[i], truths[i]],
                    ax.get_ylim(), color='C0', lw=1)

        # Move axis and add label
        ax.yaxis.tick_right()
        if zscaling == 'max':
            ax.set_ylabel('Scaled PDF')
        elif zscaling == 'count':
            ax.set_ylabel(r'N')
        elif zscaling == 'frac':
            ax.set_ylabel('Fraction')
        elif zscaling == 'density':
            ax.set_ylabel('Density')
        elif zscaling == 'normed':
            ax.set_ylabel('PDF')
        ax.yaxis.set_label_position('right')

        # Set x labels
        if i == ndim-1:
            if labels is not None:
                ax.set_xlabel(labels[i])
            plt.setp(ax.xaxis.get_majorticklabels(), rotation=60)
        else:
            ax.set_xticklabels([])

        # Loop in j direction
        for j in range(i+1,ndim):

            # Grab axes for this pair
            ax = plt.subplot(gs[j, i])
            
            # Define y grid and 2d grid
            y = pts[:,j]
            ylim = np.zeros(2)
            if hasattr(nbin, '__iter__'):
                nybin = nbin[j]
            else:
                nybin = nbin
            if lolim is None:
                ylim[0] = np.amin(y)
            else:
                ylim[0] = lolim[j]
            if hilim is None:
                ylim[1] = np.amax(y)
            else:
                ylim[1] = hilim[j]
            ygrd = np.linspace(ylim[0], ylim[1], nybin+1)
            ygrd_h = 0.5*(ygrd[1:]+ygrd[:-1])
            xx, yy = np.meshgrid(xgrd_h, ygrd_h)

            # If using logarithmic scaling, apply it
            z = zarr[ctr]
            if log:
                z[z == 0] = 1.0e-6*np.amin(z[z > 0])
                z[z == 0] = np.amin(z[z > 0])
                z = np.log10(z)
                
            # Plot contour at threshhold
            ax.contour(xx, yy, np.transpose(z),
                       levels=[zmin],
                       colors='k', linestyles='-')
    
            # Plot scatter points outside contour
            ax.scatter(x[scatteridxarr[ctr]],
                       y[scatteridxarr[ctr]],
                       color='k', s=5, alpha=scat_alpha,
                       edgecolor='none')

            # Plot density map
            img = ax.imshow(np.transpose(z), origin='lower',
                            aspect='auto',
                            vmin=zmin, vmax=zmax,
                            cmap=cm.afmhot_r,
                            extent=[xlim[0], xlim[1], ylim[0],
                                    ylim[1]])

            # Add truths
            if truths is not None:
                ax.plot([truths[i], truths[i]],
                        ax.get_ylim(), color='C0', lw=1)
                ax.plot(ax.get_xlim(),
                        [truths[j], truths[j]],
                        color='C0', lw=1)
                ax.scatter([truths[i]], [truths[j]], marker='o',
                           color='C0', s=40)

    
            # Set plot range
            ax.set_xlim(xlim)
            ax.set_ylim(ylim)

            # Adjust labels
            if i != 0:
                ax.set_yticklabels([])
            if j != ndim-1:
                ax.set_xticklabels([])
            if labels is not None:
                if i == 0 and labels is not None:
                    ax.set_ylabel(labels[j])
                if j == ndim-1:
                    if labels is not None:
                        ax.set_xlabel(labels[i])
                    plt.setp(ax.xaxis.get_majorticklabels(), rotation=60)

            # Increment counter
            ctr = ctr + 1

    # Add final colorbar
    if colorbar:
        if zscaling == 'max':
            if log:
                label = 'log Scaled PDF'
            else:
                label = 'Scaled PDF'
        elif zscaling == 'count':
            if log:
                label = 'log N'
            else:
                label = 'N'
        elif zscaling == 'frac':
            if log:
                label = 'log fraction'
            else:
                label = 'fraction'
        elif zscaling == 'density':
            if log:
                label = 'log density'
            else:
                label = 'density'
        elif zscaling == 'normed':
            if log:
                label = 'log PDF'
            else:
                label = 'PDF'
        if ndim > 2:
            ax = plt.subplot(gs[ndim//2-1,ndim//2+1])
        else:
            # Special case for exaclty 2 dimensions
            ax = plt.subplot(gs[0,1])
        cbar = plt.colorbar(img, ax=ax, orientation='vertical',
                            fraction=0.8, aspect=10, label=label)
        if nticks is not None:
            cbar.set_ticks(np.linspace(zmin, zmax, nticks))
        ax.remove()

