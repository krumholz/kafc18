"""
This script get the properties of clusters from traditional chi^2
fitting to fully-sampled tracks.
"""

import numpy as np
from numpy.ma import MaskedArray
import glob
import os.path as osp
from slugpy import read_cluster

# Base name of track files
fitdir = 'conventional'
trackdir = 'tracks'
base_trackname = 'mock_track_av'

# List of mock catalogs
catalogs = [ 'powerlaw', 'truncated', 'mdd' ]

# List of filters
filters = ['WFC3_UVIS_F275W', 'WFC3_UVIS_F336W', 'WFC3_UVIS_F438W',
           'WFC3_UVIS_F555W', 'WFC3_UVIS_F814W']

# Get list of track files and their A_V's
trackfiles = glob.glob(
    osp.join(fitdir, trackdir, base_trackname)+'*_cluster_prop.fits')
trackfiles.sort()
av = [0.0]
for i in range(len(trackfiles)):
    trackfiles[i] = trackfiles[i][:-len('_cluster_prop.fits')]
    av.append(float(trackfiles[i][-4:]))
av = np.array(av)

# Read photometry from all tracks
data = read_cluster(trackfiles[0], read_filters=filters)
mtrack = data.target_mass[0]
time = data.time
phot = np.zeros((len(av), data.phot.shape[0], len(filters)))
phot[0,:,:] = data.phot_neb
for i in range(len(trackfiles)):
    data = read_cluster(trackfiles[i], read_filters=filters)
    phot[i+1,:,:] = data.phot_neb_ex

# Grab photometry from all catalogs
mockdir = 'mockdata'
catdata = {}
for cat in catalogs:

    # Read catalog
    data = np.loadtxt(osp.join(mockdir, "mock_"+cat+"_obs.txt"))
    catdata[cat] = { 'phot' : data[:,1:-1],
                     'cid'  : np.array(data[:,0], dtype='int') }

# The photometry in the tracks can be altered by changing the mass, which
# has the effect of adding a constant offset to each magnitude bin. To find
# the offset that minimizes chi^2, we need to find the minimum of
# sigma^2 = | m_{cat} - m_{track} - f I |^2,
# where m_{cat} is the vector of magnitudes in the catalog, m_{track} is
# the corresponding vector of track magnitudes, and I is a vector of all
# ones. The minimum occurs at
# f = 1/N (sum_i m_{i,cat} - sum_i m_{i,track})
# where N is the number of photometric bands, and m_{i,cat} is the ith
# magnitue in the catalog, and similrly for m_{i,track}. We need to compute
# f for every combination of tracks and catalog
photerr = 0.1
for c in catalogs:
    f = np.zeros(catdata[c]['phot'].shape[:-1]+phot.shape[:-1])
    for i in range(phot.shape[-1]):
        f += np.subtract.outer(catdata[c]['phot'][...,i], phot[...,i])
    f /= phot.shape[-1]
    chi2 = np.zeros(catdata[c]['phot'].shape[:-1]+phot.shape[:-1])
    for i in range(phot.shape[-1]):
        chi2 += np.transpose(catdata[c]['phot'][...,i] -
                             np.transpose(phot[...,i]+f))**2
    chi2 /= photerr**2
    catdata[c]['chi2'] = chi2
    catdata[c]['scalefac'] = f

# From the chi^2 surface, find the best fitting age and extinction, 
# and derive confidence intervals. Derive the corresponding masses
# from the scale factors.
for c in catalogs:
    ncl = catdata[c]['chi2'].shape[0]
    nt = len(time)
    nav = len(av)
    fitidx = np.argmin(catdata[c]['chi2'].reshape((ncl,nav*nt)), axis=1)
    avidx, tidx = np.unravel_index(fitidx, (nav,nt))
    
    # Best fitting age, A_V
    catdata[c]['bestchi2'] = catdata[c]['chi2'][range(ncl),avidx,tidx]
    catdata[c]['logtfit'] = np.log10(time[tidx])
    catdata[c]['avfit'] = av[avidx]
    
    # Corresponding mass
    f = catdata[c]['scalefac'][range(ncl),avidx,tidx]
    catdata[c]['logmfit'] = np.log10(mtrack) - f/2.5
    
    # 68% confidence interval; numerical value 2.3 is from Avni (1961)
    chi2_68 = catdata[c]['bestchi2'] + 2.3
    idx = np.transpose(np.transpose(catdata[c]['chi2']) < chi2_68)
    tidx = np.logical_or.reduce(idx, axis=1)
    avidx = np.logical_or.reduce(idx, axis=2)
    tloidx = np.argmax(tidx, axis=1)
    thiidx = tidx.shape[-1] - np.argmax(tidx[:,::-1], axis=1)-1
    avloidx = np.argmax(avidx, axis=1)
    avhiidx = avidx.shape[-1] - np.argmax(avidx[:,::-1], axis=1)-1
    catdata[c]['logtlo'] = np.log10(time[tloidx])
    catdata[c]['logthi'] = np.log10(time[thiidx])
    catdata[c]['avlo'] = av[avloidx]
    catdata[c]['avhi'] = av[avhiidx]
    
    # Get 68% confidence interval in mass, which we take to be the
    # minimum and maximum masses that have chi^2 in the acceptable range;
    # note that we cannot assume that mass is monotonic with T or A_V
    fmask = MaskedArray(catdata[c]['scalefac'], mask=np.logical_not(idx))
    fmin = np.amin(fmask, axis=(1,2))
    fmax = np.amax(fmask, axis=(1,2))
    catdata[c]['logmlo'] = np.array(np.log10(mtrack) - fmax/2.5)
    catdata[c]['logmhi'] = np.array(np.log10(mtrack) - fmin/2.5)

# Write fits to file
outdir = 'conventional'
for c in catalogs:
    fp = open(osp.join(outdir, c+'_fit.txt'), 'w')
    fp.write("#   ID")
    fp.write("   {:>15s}".format("logMlo"))
    fp.write("   {:>15s}".format("logMfit"))
    fp.write("   {:>15s}".format("logMhi"))
    fp.write("   {:>15s}".format("logTlo"))
    fp.write("   {:>15s}".format("logTfit"))
    fp.write("   {:>15s}".format("logThi"))
    fp.write("   {:>15s}".format("AVlo"))
    fp.write("   {:>15s}".format("AVfit"))
    fp.write("   {:>15s}".format("AVhi"))
    fp.write("   {:>15s}\n".format("RedChiSqr"))
    for i in range(catdata[c]['logmfit'].size):
        fp.write(("{:>6d}   "
                  "{:>15.9f}   {:>15.9f}   {:>15.9f}   "
                  "{:>15.9f}   {:>15.9f}   {:>15.9f}   "
                  "{:>15.9f}   {:>15.9f}   {:>15.9f}   "
                  "{:>15.9f}\n").format(
            catdata[c]['cid'][i],
            catdata[c]['logmlo'][i], 
            catdata[c]['logmfit'][i],
            catdata[c]['logmhi'][i],
            catdata[c]['logtlo'][i], 
            catdata[c]['logtfit'][i],
            catdata[c]['logthi'][i],
            catdata[c]['avlo'][i], 
            catdata[c]['avfit'][i],
            catdata[c]['avhi'][i],
            catdata[c]['bestchi2'][i]/2.0
        ))
    fp.close()
