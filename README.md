### SLUG Cluster Bayesian Forward-Modelling Pipeline ###

This repository contains codes and scripts that use SLUG to perform
Bayesian forward-modelling of the demographics of star clusters, using
the method described in Krumholz, Adamo, Fumagalli, & Calzetti (2018).

### Before starting ###

Before you can use this pipeline, you will need to carry out two
steps.

1. Install [slug](https://bitbucket.org/krumholz/slug2). This software
makes use of the [slug code](https://bitbucket.org/krumholz/slug2),
and you will need to have that installed before you can begin to any
of the code in this repository except the plotting scripts.

2. Install [emcee](https://github.com/dfm/emcee). This code also uses
Daniel Foreman-Mackey's [emcee](https://github.com/dfm/emcee)
package. Install that too.

3. Copy the libraries. The analysis code uses a pre-generated library
of cluster models. The slug code can generate a new one as needed, but
for convenience the libraries used in the paper are provided. For
reasons of space they are not included in the bitbucket
repository. Instead, they are available from
<http://www.mso.anu.edu.au/~krumholz/data/kafc18.tgz>. Download and
then unzip the tarball (*warning: the tarball is 23 GB in size*),
which will create a directory called kafc18 containing the libraries
and the metadata that goes with them. In order to run the various
analysis scripts you will need to specify the library location.

### Repository workflow ###

This repository contains the code necessary to carry out all the steps
described in the Krumholz et al. (2018) paper. Note that, because all
the data are provided already, you can skip any of these steps and
just rely on the data files that are already provided. Briefly, the
analys steps are:

1. Generation of mock data using slug. In order to generate the mock
catalogs, cd into the `mockdata` directory. There you will find the
files `mock_mdd.param`, `mock_powerlaw.param`, `mock_mismatch.param`,
and `mock_truncated.param`. These are slug parameter files. To
generate the corresponding cluster catalogs, run slug on each
parameter file, i.e.,

`slug mock_mdd.param`

Then run the script `make_catalogs.py` in the main directory. This
will generate the remainder of the files in the `mockdata` directory,
which contain the catalogs pruned for observational completeness.

2. Generation of the MCMC chains that describe the posterior
probability distribution. To carry out this step, run the
`analyze_catalog.py` script in the main directory with the appropriate
arguments for each mock catalog. The `scripts` directory contains PBS
scripts that carry out the necessary runs. To use these, you will
almost certainly need to update them to the file paths on your own
system, but you can look at them to see what arguments should be
passed to `analyze_catalog.py` to reproduce the analysis in the paper.

3. Carry out the conventional chi^2 analysis. To accomplish this, use
the script `fit_catalogs.py` in the main directory. This will
automatically do chi^2 fitting on all catalogs using the method
described in the paper, and generate the files in the `conventional`
sub-directory, which contain the results of the chi^2 fits.

4. Generate plots. The scripts `plot_binned_dist.py`, `plot_mock.py`,
and `plot_posteriors.py` generate all the plots that appear in the
paper. 


### Layout of the repository ###

The full contents of the repository are as follows:

* *analyze_catalog.py*: this is the main analysis script. It performs
   the MCMC optimisation that generates the chains
* *catalog_reader.py*: this module contains code to read catalogs of
   star clusters to be fit
* *clean_legus.py*: a script to do some data cleanup on catalogs in
   the LEGUS format; not used in the current version of the
   repository, but included for future expansions
* *completeness.py*: this module contains code to calculate the
   completeness of catalogs
* *fit_catalogs.py*: this script carries out the conventional chi^2
   fitting of the catalogs
* *make_catalogs.py*: this script reads the output of the slug runs
   that generate the mock catalogs and produces catalogs that are
   pruned based on observational completeness
* *npdfplot.py*, *pdfplot.py*: these modules contain plotting
   utilities
* *plot_binned_dist.py*, *plot_mock.py*, *plot_posteriors.py*: these
   scripts generate all the plots in the paper
* *chains/*: this directory contains the output MCMC chains that
   result from running the analysis scripts
* *conventional/*: this directory contains the output of the
   conventional fitting script
* *conventional/tracks/*: this directory contains the set of
   evolutionary tracks used in the conventional fitting code
* *mockdata/*: this directoy contains the mock catalogs, and all the
   files necessary to generate them
* *scripts/*: this directory contains scripts that run
   `analyze_catalog.py` to perform the MCMC analysis