"""
This module defines the completeness class, whose purpose is to take
as input a set of photometry measurements, and return as output the
estimated completeness for that set of photometric parameters. The
module defines a generic interface for the class, and then specializes
to various cases. Users can define their own specializations in an
analogous way, and modify the analyze_catalog script to use them.
"""

import numpy as np

###########################################
# This defines the completeness interface #
###########################################
class completeness(object):
    """
    This is a helper class to compute observational completeness from
    photometry.

    Parameters
       filterset : list of string
          a list giving the names of the filters used for photometry
    """
    def __init__(self, filterset=None):
        """
        This instantiates a completeness object

        Parameters
           filterset : list of string
              a list giving the names of the filters used for
              photometry
        """
        self.filterset = filterset

    def comp(self, phot):
        """
        This method takes an array of photometry as input, and returns
        the estimated completeness at each point in photometric space

        Parameters
           phot : array, shape (N, M)
              array of N photometric values measured in M filters

        Returns
           comp : array, shape (N)
              estimated completeness at each photometric point
        """
        raise NotImplementedError(
            "completeness is an abstract class; "
            "implement a derived class")

    
##############################################################
# This is a specialization of the interface to the test mock #
# catalogs                                                   #
##############################################################

class completeness_mock(completeness):
    """
    This is a specialization of the completeness class to the
    completeness function used for the mock catalog tests.
    """
    def __init__(self,
                 filterset = ['WFC3_UVIS_F275W', 'WFC3_UVIS_F336W',
                              'WFC3_UVIS_F438W', 'WFC3_UVIS_F555W',
                              'WFC3_UVIS_F814W']):
        __doc__ == completeness.__init__.__doc__
        try:
            # Python 3 version
            super().__init__(filterset = filterset)
        except:
            # Python 2 version
            super(completeness_mock, self).__init__(filterset = filterset)
        if not 'WFC3_UVIS_F555W' in self.filterset:
            raise ValueError("completeness_mock requires "
                             "that the filter set include "
                             "WFC3_UVIS_F555W")
    def comp(self, phot):
        __doc__ == completeness.comp.__doc__
        Vmag = phot[:, self.filterset.index('WFC3_UVIS_F555W')]
        c = np.zeros(np.asarray(Vmag).shape)
        idx1 = np.asarray(Vmag) <= -5
        c[idx1] = 1.0
        idx2 = np.asarray(Vmag) >= -4
        c[idx2] = 0.0
        idx = np.logical_and(np.logical_not(idx1),
                             np.logical_not(idx2))
        c[idx] = -4.0 - np.asarray(Vmag)[idx]
        return c

######################################################################
# These is class implements completeness for LEGUS catalogs. The     #
# class is instantiated by providing a table of measured             #
# completeness in each input band, which will be different for       #
# different LEGUS galaxies. Values for measured galaxies are hard-   #
# coded below.                                                       #
######################################################################
    
class completeness_LEGUS(completeness):
    """
    This is a specialization of the completeness class to the
    LEGUS catalog.
    """
    def __init__(self, filterset, phot_tab, comp_tab, viscat=False):
        """
        This instantiates a completeness object

        Parameters
           filterset : list of string
              a list giving the names of the filters used for
              photometry
           phot_tab : arraylike, shape (N)
              this specifies the magnitudes at which completeness is
              measured
           comp_tab : arraylike, shape (M, N)
              the measured completeness at each of the N magnitudes in
              phot_tab, for each of the M filters in filterset
           viscat : bool
              compute completeness fraction for visual catalog or for
              automated catalog; viscat = True means for visual catalog
        """

        # Store filter names
        try:
            # Python 3
            super().__init__(filterset=filterset)
        except:
            # Python 2
            super(completeness_LEGUS, self).__init__(filterset=filterset)
        
        # Identify the B, V, and I filters for this filter set
        self.v_idx = None
        self.i_idx = None
        self.b_idx = None
        for i, f in enumerate(self.filterset):
            if 'F555W' in f:
                self.v_idx = i
            if 'F814W' in f:
                self.i_idx = i
            if 'F435W' in f or 'F438W' in f:
                self.b_idx = i
        if self.v_idx is None:
            raise ValueError("completeness_LEGUS requires "
                             "that the filter set include "
                             "a V filter")

        # Store tabulated completeness
        self.phot_tab = phot_tab
        self.comp_tab = comp_tab

        # Store catalog type
        self.viscat = viscat

    # Function to compute the completeness in a particular filter
    def comp_filter(self, phot, filt):
        """
        Return the completeness fraction in a particular filter

        Parameters:
           phot : arraylike
              photometric values (usually absolute magnitude) in the
              specified filter
           filt : int or str
              filter in which phot is measured; if filt is an integer, it
              is interpreted as an index in the filter set for this
              object; if it is a string, it is interpreted as the name of
              a filter

        Returns
           comp : array
              completeness in the specified filter
        """

        # Get index of filter
        if type(filt) is str:
            i = self.filterset.index(filt)
        else:
            i = filt

        # Put input photometric values into bins in the table
        idx = np.digitize(phot, self.phot_tab)

        # Do linear interpolation within the table
        out = np.zeros(len(np.atleast_1d(phot)))
        j = np.logical_and(idx > 0, idx < self.phot_tab.size)
        wgt = (np.atleast_1d(phot)[j] - self.phot_tab[idx[j]-1]) / \
              (self.phot_tab[idx[j]] - self.phot_tab[idx[j]-1])
        out[j] = (1.0-wgt) * self.comp_tab[i,idx[j]-1] + \
                        wgt * self.comp_tab[i,idx[j]]

        # Extrapolate for points off table; clip to keep values in
        # range 0 - 1
        j = idx == 0
        if np.amax(j):
            slope = (self.comp_tab[i,1] - self.comp_tab[i,0]) / \
                    (self.phot_tab[1] - self.phot_tab[0])
            out[j] = self.comp_tab[i,0] + \
                     slope * (np.atleast_1d(phot)[j] - self.phot_tab[0])
        j = idx == self.phot_tab.size
        if np.amax(j):
            slope = (self.comp_tab[i,-1] - self.comp_tab[i,-2]) / \
                    (self.phot_tab[-1] - self.phot_tab[-2])
            out[j] = self.comp_tab[i,-1] + \
                     slope * (np.atleast_1d(phot)[j] - self.phot_tab[-1])
        out = np.clip(out, 0, 1)

        # Return
        return out
    

    # The completeness function
    def comp(self, phot):
        __doc__ == completeness.comp.__doc__

        # Compute the probability of detection in each filter
        # separately
        pdetect = np.zeros(np.atleast_2d(phot).shape)
        nf = len(self.filterset)
        for i in range(nf):
            pdetect[...,i] = self.comp_filter(
                np.atleast_2d(phot)[...,i], i)

        # Compute probability of inclusion in the catalog at each
        # input point in photometric space. Inclusion in the visual
        # catalog requires that:
        #
        # (1) the cluster is detected in V
        # (2) it is detected in at least 3 other bands
        # (3) the V magnitude is < -6
        #
        # while inclusion in the automatic catalog only requires that
        #
        # (1) the cluster is detected in V
        # (2) the cluster is detected in B or I
        #
        # Note that, since there are at most 5 bands in total, any
        # cluster that satisfies visual catalog conditions (1) and (2)
        # automatically satisfies automatic catalog condition (2), so
        # there is no need to check for it separately.
        #
        # We compute the probability of (1) and (2) in both cases by
        # looping over all 2^nf possible combinations of detection
        # and non-detection in each filter, and adding up the
        # probabilities of the cases that lead to inclusion in the
        # catalog.
        out = np.zeros(pdetect.shape[:-1])
        for i in range(2**nf):
            # i is interpreted bitwise, with a 1 in a given bit
            # corresponding to a detection in that band

            # Check condition (1): detection in V
            v_detect = i & (1 << self.v_idx)
            if not v_detect:
                continue

            # Check condition (2)
            if self.viscat:
                ndetect = bin(i).count('1')
                if ndetect < 3:
                    continue
            else:
                if self.b_idx is not None:
                    b_detect = i & (1 << self.b_idx)
                else:
                    b_detect = False
                if self.i_idx is not None:
                    i_detect = i & (1 << self.i_idx)
                else:
                    i_detect = False
                if not (b_detect or i_detect):
                    continue

            # If we're here, (1) and (2) are satisfied in this case,
            # so add its probability
            prob = np.ones(np.atleast_2d(phot).shape[:-1])
            for j in range(nf):
                if i & (1 << j):
                    prob *= pdetect[...,j]
                else:
                    prob *= 1.0 - pdetect[...,j]
            out += prob

        # If using the visual catalog, set comp = 0 for clusters with
        # V > -6
        if self.viscat:
            out[phot[...,self.v_idx] > -6] = 0.0

        # Return completeness
        return out



###############################################################        
# Data for LEGUS fields where artificial star tests have been #
# performed; citations are given below                        #
###############################################################        
        
LEGUS_field_data = {
    
    # Data for NGC 628c from Adamo et al., 2017, ApJ, 841, 131
    'ngc628c' : {
        'filterset' : ['WFC3_UVIS_F275W', 'WFC3_UVIS_F336W',
                       'ACS_F435W', 'ACS_F555W', 'ACS_F814W' ],
        'phot_tab' : 20.25 + 0.5*np.arange(12) - 29.98,
        'comp_tab' : np.array([
            # F275W
            [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.95,
             0.36, 0.039, 0.0, 0.0, 0.0],
            # F336W
            [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
             0.97, 0.74, 0.17, 0.0, 0.0],
            # F435W
            [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
             1.0, 1.0, 0.98, 0.77, 0.36],
            # F555W
            [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
             1.0, 1.0, 1.0, 0.83, 0.33],
            # F814W
            [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
             1.0, 0.91, 0.49, 0.11, 0.010]
            ])
        },
    
    # Data for NGC 628c from Adamo et al., 2017, ApJ, 841, 131
    'ngc628e' : {
        'filterset' : ['WFC3_UVIS_F275W', 'WFC3_UVIS_F336W',
                       'ACS_F435W', 'WFC3_UVIS_F555W', 'ACS_F814W' ],
        'phot_tab' : 20.25 + 0.5*np.arange(12) - 29.98,
        'comp_tab' : np.array([
            # F275W
            [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.95,
             0.33, 0.0, 0.0, 0.0, 0.0],
            # F336W
            [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
             0.69, 0.11, 0.0, 0.0, 0.0],
            # F435W
            [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
             1.0, 1.0, 0.95, 0.91, 0.30],
            # F555W
            [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
             1.0, 1.0, 1.0, 0.88, 0.26],            
            # F814W
            [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
             1.0, 1.0, 0.59, 0.15, 0.01]
        ])            
    }
    }
        
##############################################################
# Here we register the list of known completeness functions; #
# note that the register entry "LEGUS" is an abstract class  #
# that must be completed by instantiating it with a filter   #
# set and completeness data                                  #
##############################################################

comp_register = {
    'mock' : completeness_mock(),
    'LEGUS' : completeness_LEGUS,
    'LEGUS_ngc628c_vis' : completeness_LEGUS(
        LEGUS_field_data['ngc628c']['filterset'],
        LEGUS_field_data['ngc628c']['phot_tab'],
        LEGUS_field_data['ngc628c']['comp_tab'],
        viscat=True),
    'LEGUS_ngc628c_auto' : completeness_LEGUS(
        LEGUS_field_data['ngc628c']['filterset'],
        LEGUS_field_data['ngc628c']['phot_tab'],
        LEGUS_field_data['ngc628c']['comp_tab'],
        viscat=False),
    'LEGUS_ngc628e_vis' : completeness_LEGUS(
        LEGUS_field_data['ngc628e']['filterset'],
        LEGUS_field_data['ngc628e']['phot_tab'],
        LEGUS_field_data['ngc628e']['comp_tab'],
        viscat=True),
    'LEGUS_ngc628e_auto' : completeness_LEGUS(
        LEGUS_field_data['ngc628e']['filterset'],
        LEGUS_field_data['ngc628e']['phot_tab'],
        LEGUS_field_data['ngc628e']['comp_tab'],
        viscat=False)
}
