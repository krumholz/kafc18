SLUG WAS RUN WITH THE FOLLOWING PARAMETERS
model_name           mock_track_av0.20
out_dir              
sim_type             cluster
n_trials             1
time_step            0.01
end_time             1.001e+10
IMF                  /Users/krumholz/Projects/slug2/lib/imf/chabrier.imf
cluster_mass         1e+06
CLF                  /Users/krumholz/Projects/slug2/lib/clf/nodisrupt.clf
tracks               /Users/krumholz/Projects/slug2
atmos_dir            /Users/krumholz/Projects/slug2/lib/atmospheres
yield_dir            /Users/krumholz/Projects/slug2/lib/yields
min_stoch_mass       120
redshift             0
metallicity          1
specsyn_mode         sb99
yield_mode           SNII: Sukhbold+16; AGB: Karakas & Lugaro 2016 + Doherty+ 2014
extinction           yes
A_V                  0.2
extinction_curve     /Users/krumholz/Projects/slug2/lib/extinct/MW_EXT_SLUG.dat
nebular_emission     yes
nebular_density      100
nebular_temperature  -1
nebular_phi          0.5
nebular_logU         -3
phot_mode            Vega
out_cluster          1
out_cluster_phot     1
out_cluster_spec     0
out_cluster_ew       0
phot_bands           WFC3_UVIS_F275W, WFC3_UVIS_F336W, WFC3_UVIS_F438W, WFC3_UVIS_F555W, WFC3_UVIS_F814W, ACS_F435W, ACS_F555W, ACS_F606W, ACS_F814W, WFC3_UVIS_F657N, WFC3_UVIS_F547M, ACS_F658N, ACS_F660N, QH0, Lbol, Johnson_U, Johnson_B, Johnson_V, Cousins_R, Cousins_I
