#####################################################################
# This parameter file is a companion to the other mocks; it just
# computes the photometric evolution of a cluster with a fully-
# sampled IMF
#
#####################################################################


##############
# Basic data #
##############

# Name of the model; this will become the base name for all output
# files
model_name    mock_track_av0.30

# Level of verbosity while running; allowed values:
# -- 0 (run silently except for runtime warnings and errors)
# -- 1 (some basic output)
# -- 2 (lots of output)
verbosity        2


##################################################################
# Parameters controlling simulation execution and physical model #
##################################################################

# Type of simulation. Allowed values:
# -- cluster (simulate a simple stellar population all formed at time
#    0)
# -- galaxy (continuous star formation)
sim_type  	  cluster

# Number of models to run
n_trials          1

# Logarithmic time stepping? Allowed values:
# -- 0 (no)
# -- 1 (yes)
# Default: 0
log_time          1

# Time step in dex
time_step    	  0.01

# Starting time (in yr)
start_time        1.0e5

# Maximum evolution time, in yr.
end_time	  1.001e10

# Cluster mass -- just sets normalization in non-stochastic run
cluster_mass      1.0e6


#############################################
# Parameters controlling simulation outputs #
#############################################

# Write out cluster physical properties?
out_cluster       1

# Write out cluster photometry?
out_cluster_phot  1

# Write out cluster spectra?
out_cluster_spec  0

# Write out cluster yields?
out_cluster_yield  0

# Output type
output_mode      fits


#####################################################################
# Parameters controlling the physical models used for stars         #
#####################################################################

# IMF (initial mass function) file name
imf   	          lib/imf/chabrier.imf

# Minimum stochastic mass -- set to maximum IMF mass to force full
# non-stochasticity
min_stoch_mass	  120

# CLF (cluster lifetime function) file name; this one turns off disruption
clf               lib/clf/nodisrupt.clf

# Stellar tracks to use; allowed values are:
# -- geneva_2013_vvcrit_00 (Geneva 2013 models, non-rotating)
# -- geneva_2013_vvcrit_40 (Geneva 2013 models, rotating 40% breakup)
# -- geneva_mdot_std (Geneva pre-2013 models, standard mass loss rate)
# -- geneva_mdot_enhanced (Geneva pre-2013 models, x2 mass loss rate)
# -- padova_tpagb_yes (Padova models, added TP-AGB stars)
# -- padova_tpagb_no (Padova models, no TP-AGB stars)
# -- mist_2016_vvcrit_00 (MIST 2016 v1.0 models, non-rotating)
# -- mist_2016_vvcrit_40 (MIST 2016 v1.0 models, rotating 40% breakup)
# -- Any file name specifying a track file (generally lib/tracks/...)
# Default: geneva_2013_vvcrit_00
tracks	  	  mist_2016_vvcrit_40
#tracks		  geneva_2013_vvcrit_00
#tracks		  lib/tracks/mist/vvcrit040/MIST_v1.0_feh_p0.00_afe_p0.0_vvcrit0.4_EEPS.fits.gz

# Spectral synthesis mode
specsyn_mode	   sb99

# Metallicity relative to Solar
metallicity       1.0

#####################################################################
# Parameters controlling extinction                                 #
#####################################################################

# Extinction; to turn on extintion, set the parameter A_V. If set to
# a number, this extinction will be applied to all spectra and
# photometry. Default behavior, if the A_V parameter is omitted
# entirely, is not to apply any extinction. If the parameter A_V is
# set to a real number, this will be interpreted as a uniform
# extinction that is applied to all light output. Finally, if this
# parameter is set to any string that cannot be converted to a real
# number, it will be interpreted as a file name giving the name of a
# PDF file specifying the distribution of extinctions.
A_V    0.300000

# File specifying the shape fof the extinction curve
extinction_curve     lib/extinct/MW_EXT_SLUG.dat

# Excess nebular extinction factor
nebular_extinction_factor  lib/avdist/neb_factor_default.av


#####################################################################
# Parameters controlling the nebular emission                       #
#####################################################################

# Compute nebular emission or not
compute_nebular      1

# Fraction of ionizing photons assumed to absorbed by H and produce
# nebular emission within the observed aperture; values < 1 can
# represent either absorption by dust instead of gas, or escape of
# ionizing photons outside the observed aperture
nebular_phi          0.5

#############################################
# Parameters describing photometric filters #
#############################################

# All these parameters can be omitted if no photometric output is
# requested, i.e. if out_integrated_phot = 0 and out_cluster_phot = 0
# (see below).

# Photometric filters to be used; can be comma- or
# whitespace-separated. For a list of available filters, see the file
# lib/filters/FILTER_LIST. In addition to those filters, the following
# special values are always available:
# -- QH0 : H ionizing photon luminosity, in phot/s
# -- QHe0 : He ionizing photon luminosity, in phot/s
# -- QHe1 : He+ ionizing photon luminosity, in phot/s
# -- Lbol : bolometric luminosity, in L_sun
phot_bands  WFC3_UVIS_F275W, WFC3_UVIS_F336W, WFC3_UVIS_F438W, WFC3_UVIS_F555W, WFC3_UVIS_F814W, ACS_F435W, ACS_F555W, ACS_F606W, ACS_F814W, WFC3_UVIS_F657N, WFC3_UVIS_F547M, ACS_F658N, ACS_F660N, QH0, Lbol, Johnson_U, Johnson_B, Johnson_V, Cousins_R, Cousins_I

# Directory containing photometric filter data
# Default: lib/filters
filters	           lib/filters

# Photometry mode; allowed values are
# -- L_nu (report frequency-averaged luminosity in band, in erg/s/Hz)
# -- L_lambda (report wavelength-averaged luminosity in band, in erg/s/A)
# -- AB (report absolute AB magnitude)
# -- STMAG (report absolute ST magnitude)
# -- VEGA (report absolute Vega magnitude)
# Default: L_nu
phot_mode          VEGA


