"""
This script plots distributions of various parameters for a mock catalog
"""

import numpy as np
import os.path as osp
import matplotlib.pyplot as plt
from slugpy import read_cluster
from pdfplot import pdfplot

# Which data set to plot
datadir = 'mockdata'
mock_name = osp.join(datadir, 'mock_powerlaw')
mock_track_name = osp.join(datadir, 'mock_track')

# Filter names
filters = ['WFC3_UVIS_F275W', 'WFC3_UVIS_F336W', 'WFC3_UVIS_F438W',
           'WFC3_UVIS_F555W', 'WFC3_UVIS_F814W']

# Completeness function
def comp(Vmag):
    c = np.zeros(np.asarray(Vmag).shape)
    idx1 = np.asarray(Vmag) <= -5
    c[idx1] = 1.0
    idx2 = np.asarray(Vmag) >= -4
    c[idx2] = 0.0
    idx = np.logical_and(np.logical_not(idx1),
                         np.logical_not(idx2))
    c[idx] = -4.0 - np.asarray(Vmag)[idx]
    return c
    
# Read data physical data
data = read_cluster(mock_name)
logm = np.log10(data.actual_mass)
logt = np.log10(data.time)

# Read photometric data for all clusters
data = np.loadtxt(mock_name+"_full.txt", comments="#")
uv_mag = data[:,1]
u_mag = data[:,2]
b_mag = data[:,3]
v_mag = data[:,4]
uv_u = uv_mag - u_mag
u_b = u_mag - b_mag

# Read observed cluster list to figure out which ones are observed
data = np.loadtxt(mock_name+"_obs.txt", comments="#")
idx_obs = data[:,0].astype(int)-1

# Read the fully-sampled track for a 10^6 Msun cluster
track = read_cluster(mock_track_name)
uv_track = track.phot_neb[:,track.filter_names.index(filters[0])]
u_track = track.phot_neb[:,track.filter_names.index(filters[1])]
b_track = track.phot_neb[:,track.filter_names.index(filters[2])]
v_track = track.phot_neb[:,track.filter_names.index(filters[3])]
uv_u_track = uv_track - u_track
u_b_track = u_track - b_track

# Make plot of true physical distribution
plt.figure(1, figsize=(5,5))
plt.clf()
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
axcen, axtop, axright \
    = pdfplot(logt, logm, ylim=[2,6], xlim=[4,10], fignum=1,
              ylabel='$\log\,M$ [$M_\odot$]',
              xlabel='$\log\,T$ [yr]',
              yhistlim=[1,1e6], xhistlim=[10, 1e5],
              thresh=100.0, zmax=1e4,
              nxbin=30, nybin=30,
              nticks=5, zscaling='density', scat_alpha=0.5)
axcen.text(4.3, 5.7, 'All')
plt.subplots_adjust(top=0.95, right=0.95)

# Save
plt.savefig('mock_physdist.pdf')

# Make color-color plot
plt.figure(2, figsize=(5,5))
plt.clf()
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
axcen, axtop, axright \
    = pdfplot(uv_u, u_b, fignum=2,
              xlabel='$UV - U$ [mag]',
              ylabel='$U - B$ [mag]',
              xlim = [-1.5,3],
              ylim = [-2.5,2],
              thresh=1.0e3, zmax=1.0e5,
              nticks=5,
              nxbin=30, nybin=30,
              xhistlim=[1, 1e6],
              yhistlim=[1, 1e6],
              zscaling='density',
              scat_alpha=0.5)
axcen.plot(uv_u_track, u_b_track, lw=3, color='C0')
axcen.scatter(uv_u_track[::100], u_b_track[::100], 
              c=np.log10(track.time[::100]),
              edgecolors='k',
              s=100,
              cmap='Greys',
              zorder=3)
axcen.text(2.0, -1.7, 'All')
plt.subplots_adjust(top=0.95, right=0.95)

# Save
plt.savefig('mock_colorcolor.pdf')


# Make color-magnitude plot
plt.figure(3, figsize=(5,5))
plt.clf()
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
axcen, axtop, axright \
    = pdfplot(uv_u, v_mag, fignum=3,
              xlabel='$UV - U$ [mag]',
              ylabel='$V$ [mag]',
              xlim = [-1.5,3.5],
              ylim = [8,-18],
              thresh=1.0e2, zmax=1.0e4,
              nticks=5,
              nxbin=30, nybin=30,
              xhistlim=[1, 1e5],
              yhistlim=[1, 1e5],
              zscaling='density',
              scat_alpha=0.5)
for i in range(3):
    axcen.plot(uv_u_track, v_track+5*i, lw=3, color='C0')
    axcen.scatter(uv_u_track[::100], v_track[::100]+5*i, 
                  c=np.log10(track.time[::100]),
                  edgecolors='k',
                  s=100,
                  cmap='Greys',
                  zorder=3)
axcen.plot([-1.5, 3.5], [-4.5, -4.5], 'k--', lw=2)
axcen.text(2.0, -15, 'All')
plt.subplots_adjust(top=0.95, right=0.95)

# Save
plt.savefig('mock_colormag.pdf')


# Now repeat plots for the completeness-corrected sample
plt.figure(4, figsize=(5,5))
plt.clf()
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
axcen, axtop, axright \
    = pdfplot(logt[idx_obs], logm[idx_obs], fignum=4,
              ylim=[2,6], xlim=[4,10], 
              ylabel='$\log\,M$ [$M_\odot$]',
              xlabel='$\log\,T$ [yr]',
              yhistlim=[1,1e6], xhistlim=[10, 1e5],
              thresh=100.0, zmax=1e4,
              nxbin=30, nybin=30,
              nticks=5, zscaling='density', scat_alpha=0.5)
axcen.text(4.3, 5.7, 'Observed')
plt.subplots_adjust(top=0.95, right=0.95)
plt.savefig('mock_physdist_obs.pdf')

plt.figure(5, figsize=(5,5))
plt.clf()
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
axcen, axtop, axright \
    = pdfplot(uv_u[idx_obs], u_b[idx_obs], fignum=5,
              xlabel='$UV - U$ [mag]',
              ylabel='$U - B$ [mag]',
              xlim = [-1.5,3],
              ylim = [-2.5,2],
              nxbin=30, nybin=30,
              thresh=1.0e3, zmax=1.0e5,
              nticks=5,
              xhistlim=[1, 1e6],
              yhistlim=[1, 1e6],
              zscaling='density',
              scat_alpha=0.5)
axcen.plot(uv_u_track, u_b_track, lw=3, color='C0')
axcen.scatter(uv_u_track[::100], u_b_track[::100], 
              c=np.log10(track.time[::100]),
              edgecolors='k',
              s=100,
              cmap='Greys',
              zorder=3)
axcen.text(2.0, -1.7, 'Observed')
plt.subplots_adjust(top=0.95, right=0.95)
plt.savefig('mock_colorcolor_obs.pdf')

plt.figure(6, figsize=(5,5))
plt.clf()
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
axcen, axtop, axright \
    = pdfplot(uv_u[idx_obs], v_mag[idx_obs], fignum=6,
              xlabel='$UV - U$ [mag]',
              ylabel='$V$ [mag]',
              xlim = [-1.5,3.5],
              ylim = [8,-18],
              thresh=1.0e2, zmax=1.0e4,
              nxbin=30, nybin=30,
              nticks=5,
              xhistlim=[1, 1e5],
              yhistlim=[1, 1e5],
              zscaling='density',
              scat_alpha=0.5)
for i in range(3):
    axcen.plot(uv_u_track, v_track+5*i, lw=3, color='C0')
    axcen.scatter(uv_u_track[::100], v_track[::100]+5*i, 
                  c=np.log10(track.time[::100]),
                  edgecolors='k',
                  s=100,
                  cmap='Greys',
                  zorder=3)
axcen.plot([-1.5, 3.5], [-4.5, -4.5], 'k--', lw=2)
axcen.text(2.0, -15, 'Observed')

# Save
plt.subplots_adjust(top=0.95, right=0.95)
plt.savefig('mock_colormag_obs.pdf')

# Print out some statistics
mcl = 300.0
compcut = [-5, -4.5, -4]
tcomp = [track.time[
    np.argmax(v_track + 2.5*np.log10(1e6/mcl) > c)]/1e6 for c in compcut]
idx_nobs = list(set(range(len(v_mag))) - set(idx_obs))
print(("For a cluster of mass {:f}, ages corresponding to "
       "(100%, 50%, 0%) completeness are ({:f}, {:f}, {:f} Myr").
      format(mcl, tcomp[0], tcomp[1], tcomp[2]))
print("Number of observed clusters with mass < {:f}, age > {:f} = {:d}"
      .format(
          mcl, tcomp[-1],
          np.sum(np.logical_and(
          logt[idx_obs] > np.log10(tcomp[-1]*1e6),
          logm[idx_obs] < np.log10(mcl)))))
print("Number of unobserved clusters with mass > {:f}, age < {:f} = {:d}"
      .format(
          mcl, tcomp[0],
          np.sum(np.logical_and(
          logt[idx_nobs] < np.log10(tcomp[0]*1e6),
          logm[idx_nobs] > np.log10(mcl)))))
print("Number of clusters with V mag > -6 = {:d}".
      format(int(np.sum(v_mag[idx_obs] >-6))))
print("Number of clusters with V mag < -6 = {:d}".
      format(int(np.sum(v_mag[idx_obs] <= -6))))
