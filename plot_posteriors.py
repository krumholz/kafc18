"""
This script makes corner plots for the posterior distributions of the
mock catalogs, and prints out summary statistics for them.
"""

# Libraries
import numpy as np
import matplotlib.pyplot as plt
import os.path as osp
from scipy.special import erf
from npdfplot import npdfplot

# Read MCMC chains for all models
chaindir = 'chains'
catalogs = ['powerlaw', 'truncated', 'mdd', 'mismatch']
models = ['mid', 'mdd']
chains = {}
for c in catalogs:
    chainmods = {}
    for m in models:
        chaindata = np.loadtxt(osp.join(chaindir, 'mock_'+c+'_obs_'+m+'.chain'))
        nwalkers = int(np.amax(chaindata[:,0]))+1
        ndim = chaindata.shape[1] - 2
        niter = chaindata.shape[0]//nwalkers
        pos = chaindata[:,1:-1].reshape((niter,nwalkers,ndim))
        lnprob = chaindata[:,-1].reshape((niter,nwalkers))
        chainmods[m] = { 'pos' : pos, 'lnprob' : lnprob,
                         'niter' : niter, 'nburn' : niter-300 }
    chains[c] = chainmods

# Compute the AIC and Akake weights for each model and catalog
for c in catalogs:
    aicmin = np.inf
    for m in models:
        aic = 22 - 2 * np.amax(chains[c][m]['lnprob'])
        chains[c][m]['AIC'] = aic
        if aic < aicmin:
            aicmin = aic
    wsum = 0.0
    for m in models:
        chains[c][m]['Delta'] = chains[c][m]['AIC']-aicmin
        wsum += np.exp(-chains[c][m]['Delta']/2.0)
    for m in models:
        chains[c][m]['Awgt'] \
            = np.exp(-chains[c][m]['Delta']/2.0) / wsum

# Print weights
for c in catalogs:
    print("Catalog "+c+":")
    for m in models:
        print("     Model "+m+": w = {:e}".
              format(chains[c][m]['Awgt']))

# Limits, truths, and burn-in times for plots of posteriors
lolim = { 'powerlaw' : [-2.2, 5.0, -1.2, 6.3], 
          'truncated' : [-2.2, 4.5, -1.2, 7.7], 
          'mdd' : [-2.2, 4.5, 0.45, 6.5],
          'mismatch' : [-2.2, 4.5, -1.2, 7.7] }
hilim = { 'powerlaw' : [-1.8, 8.0, -0.8, 6.8], 
          'truncated' : [-1.8, 5.5, -0.8, 8.3], 
          'mdd' : [-1.8, 7.0, 0.75, 7.5],
          'mismatch' : [-1.8, 5.5, -0.8, 8.3] }
truths = { 'powerlaw' : [-2.0, 6.5, -1.0, 6.5],
           'truncated' : [-2.0, 5.0, -1.0, 8.0],
           'mdd' : [-2.0, 5.0, 0.65, 6.98],
           'mismatch' : [-2.0, 5.0, -1.0, 8.0] }

# Print percentiles for parameters
mid_params = ['alphaM', 'logMb', 'alphaT', 'logTmid']
mid_params_latex = [r'$\alpha_M$',
                    r'$\log\,M_{\mathrm{break}}$ [$M_\odot$]',
                    r'$\alpha_T$',
                    r'$\log\,T_{\mathrm{mid}}$ [yr]']
mdd_params = ['alphaM', 'logMb', 'gamma_mdd', 'logTmddmin']
mdd_params_latex = [r'$\alpha_M$',
                    r'$\log\,M_{\mathrm{break}}$ [$M_\odot$]',
                    r'$\gamma_{\mathrm{mdd}}$',
                    r'$\log\,T_{\mathrm{mdd,min}}$ [yr]']
for c in catalogs:
    if chains[c]['mid']['Awgt'] > 0.5:
        mod = 'mid'
        params = mid_params
    else:
        mod = 'mdd'
        params = mdd_params
    print("Catalog "+c+", best model is {:s}, parameters:".format(mod))
    for i in range(len(params)):
        pos = chains[c][mod]['pos']
        pct = np.percentile(pos[chains[c][mod]['nburn']:,:,i],
                            [50.*(1.0-erf(1.0/np.sqrt(2))),
                             50,
                             50.*(1.0+erf(1.0/np.sqrt(2)))])
        print("     "+params[i]+": {:f} - {:f} + {:f}"
              .format(pct[1], pct[1]-pct[0], pct[2]-pct[1]))

# Font choice
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

# Make plots
for i, c in enumerate(catalogs):
    
    # Decide which model to plot
    if chains[c]['mid']['Awgt'] > 0.5:
        mod = 'mid'
        params = mid_params_latex
    else:
        mod = 'mdd'
        params = mdd_params_latex
        
    # Get samples to plot
    npar = len(params)
    nwalkers = chains[c][mod]['pos'].shape[1]
    samples = chains[c][mod]['pos'][chains[c][mod]['nburn']:,:,:npar].\
              reshape((nwalkers*(chains[c][mod]['niter']-
                                 chains[c][mod]['nburn']),npar))

    # Make plot
    fig = plt.figure(i+1, figsize=(6,6))
    plt.clf()
    npdfplot(samples, fignum=i+1, 
             thresh=10.**-2,
             scat_alpha=0.2, 
             nbin=30,
             lolim=lolim[c],
             hilim=hilim[c],
             histlim=[[0.01,2]]*npar,
             nticks=5,
             truths=truths[c],
             labels=params)
    fig.subplots_adjust(wspace=0.175, hspace=0.125, 
                        bottom=0.13, top=0.95, left=0.13,
                        right=0.87)
    
    # Save
    plt.savefig('posteriors_'+c+'.pdf')
